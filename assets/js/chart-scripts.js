var ctx = document.getElementById('chart').getContext('2d');

var gradient = ctx.createLinearGradient(0, 0, 0, 350);
    gradient.addColorStop(0, '#0099FF');   
    gradient.addColorStop(1, 'rgba(0, 153, 255, 0)');

var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь'],
        datasets: [{
            backgroundColor: gradient,
            borderColor: '#fff',
            borderWidth: 1,
            data: [
                '6000',
                '5500',
                '8000',
                '6000',
                '6000',
                '5000',
            ],
            pointRadius: 0,  
            fill: true
        }]
    },
    options: {
        responsive: true,
        scales: {
            xAxes: [{
                gridLines: {
                    display:false
                },
                ticks: {
                    fontSize: 16,
                    fontColor: '#fff',
                    max: 62
                }
            }],
            yAxes: [{
                gridLines: {
                    display:false
                },
                ticks: {
                    fontSize: 16,
                    fontColor: '#fff'
                }
            }]
        },
        legend: {
            display: false
        },
        tooltipTemplate: "<%if (label){%><%=label + ' hod' %>: <%}%><%= value + '°C' %>",
    }
});