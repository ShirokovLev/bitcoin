$(document).ready(function(){
    $(".js-range-slider").ionRangeSlider({
        skin: "big",
        type: "single",
        min: 0,
        max: 1000,
        from: 0,
        grid: false,
        hide_min_max: true,
        postfix: '$',
        onChange: function (data) {
            // Called every time handle position is changed
    
            $('.form_type_income .form-output span').html(data.from + '$');
        },
    });

    $(window).scroll(function(){
        if($(this).scrollTop() > $('.big-graph').scrollTop()){
            $('.big-graph__line').addClass('big-graph__line_animated');
        }

        
    });

    $('.payments-slider').slick({
        slidesToShow: 6,
        prevArrow: '.payments-section__nav-arrow_prev',
        nextArrow: '.payments-section__nav-arrow_next',
        responsive: [
            {
              breakpoint: 990,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 1
              }
            },
            {
                breakpoint: 767,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1
                }
              }
        ]
    });

    // password trigger

    $('.input-block__password-trigger').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('input-block__password-trigger_active');
        if( $(this).hasClass('input-block__password-trigger_active') ){
            $(this).parent().find('.input-block__input').attr('type', 'text');
        } else {
            $(this).parent().find('.input-block__input').attr('type', 'password');
        }
        
    });

    // end password trigger

    // language trigger

    $('.language-switcher__actual').on('click',function(e){
        e.preventDefault();
        $(this).toggleClass('language-switcher__actual_active');
        $('.language-switcher__dropdown').toggleClass('language-switcher__dropdown_active');
    });

    // end language trigger

    // select

    $('.select__current').on('click', function(){
        $(this).toggleClass('select__current_active');
        $(this).next('.select__options').toggleClass('select__options_active');
    });

    $('.select__option').on('click', function(){
        var current = $(this).find('.trader-info').html();
        $(this).parents('.select').find('.select__current').find('.trader-info').html(current);
        $(this).parents('.select').find('.select__current').removeClass('select__current_active');
        $(this).parents('.select__options').removeClass('select__options_active');
    });

    // end select

    // xs media

    $('.hamburger').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('is-active');
        $('.header-xs').toggleClass('header-xs_active');
        $('body').toggleClass('body-ofh');
    });

    // end xs media

    // modals

    $('a[data-target="push-modal"]').on('click', function(e){
        e.preventDefault();
        var modalId = $(this).attr('data-target');
        $('#'+modalId).toggleClass('push-notification_active');
    });

    $('.push-notification__close').on('click', function(e){
        e.preventDefault();
        $(this).parent().removeClass('push-notification_active');
    });

    // end modals
    
});